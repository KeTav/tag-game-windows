﻿using System;

namespace TagGameWindows_.NET_4._7._2_Console_
{
    class Program
    {
        static Random rnd = new Random();
        static int[,] map = new int[4, 4];
        static int coll, row, step;
        static DateTime beginTime;
        static TimeSpan resultTime;

        static void Init()
        {
            for (int i = 0; i < 4; ++i)
                for (int j = 0; j < 4; ++j)
                    map[i, j] = (i * 4 + j + 1) % 16;
            row = 3;
            coll = 3;
        }

        static void Swap(int r1, int c1, int r2, int c2) => (map[r1, c1], map[r2, c2]) = (map[r2, c2], map[r1, c1]);

        static void ToLeft()
        {
            if (coll < 3)
                Swap(row, coll, row, ++coll);
        }

        static void ToRight()
        {
            if (coll > 0)
                Swap(row, coll, row, --coll);
        }

        static void ToUp()
        {
            if (row < 3)
                Swap(row, coll, ++row, coll);
        }

        static void ToDown()
        {
            if (row > 0)
                Swap(row, coll, --row, coll);
        }

        static void Mix()
        {
            for (int i = 0; i < 1000; ++i) {
                switch (rnd.Next(4)) {
                    case 0:
                        ToLeft();
                        break;
                    case 1:
                        ToRight();
                        break;
                    case 2:
                        ToUp();
                        break;
                    case 3:
                        ToDown();
                        break;
                }
            }
        }

        static void Print()
        {
            Console.WriteLine("Tag Game:");
            Console.WriteLine();
            for (int i = 0; i < 4; ++i) {
                for (int j = 0; j < 4; ++j) {
                    if (map[i, j] != 0) {
                        Console.Write("┌────┐");
                    } else {
                        Console.Write("      ");
                    }

                }
                Console.WriteLine();
                for (int j = 0; j < 4; ++j) {
                    if (map[i, j] == 0) {
                        Console.Write(new string(' ', 6));
                    } else {
                        Console.Write('│' + map[i, j].ToString().PadLeft(3) + " │");
                    }
                }
                Console.WriteLine();
                for (int j = 0; j < 4; ++j) {
                    if (map[i, j] != 0) {
                        Console.Write("└────┘");
                    } else {
                        Console.Write("      ");
                    }

                }
                Console.WriteLine();
            }
        }

        static bool Win()
        {
            for (int i = 0; i < 4; ++i)
                for (int j = 0; j < 4; ++j)
                    if (map[i, j] != ((i * 4 + j + 1) % 16))
                        return false;
            return true;
        }

        static void Main(string[] args)
        {
            Init();
            Mix();
            Print();
            beginTime = DateTime.Now;
            do {
                ++step;
                var key = Console.ReadKey().Key;
                switch (key) {
                    case ConsoleKey.LeftArrow:
                        ToLeft();
                        break;
                    case ConsoleKey.RightArrow:
                        ToRight();
                        break;
                    case ConsoleKey.UpArrow:
                        ToUp();
                        break;
                    case ConsoleKey.DownArrow:
                        ToDown();
                        break;
                }
                Console.Clear();
                Print();
            } while (!Win());
            resultTime = DateTime.Now - beginTime;
            Console.WriteLine();
            Console.WriteLine("You won {0} steps for {1}:{2}!!!", step, resultTime.Minutes, resultTime.Seconds);
            Console.ReadKey();
        }
    }
}
