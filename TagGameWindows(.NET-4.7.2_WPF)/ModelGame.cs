﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TagGameWindows_.NET_4._7._2_WPF_
{
    class ModelGame
    {
        Random rnd = new Random();
        int[,] map = new int[4, 4];
        int coll, row, step;

        public ModelGame()
        {
            Init();
        }

        public int GetStep => step;

        public int GetCell(int row, int coll) => map[row, coll];

        public void Init()
        {
            for (int i = 0; i < 4; ++i)
                for (int j = 0; j < 4; ++j)
                    map[i, j] = (i * 4 + j + 1) % 16;
            row = 3;
            coll = 3;
            Mix();
            step = 0;
        }

        void Swap(int r1, int c1, int r2, int c2) => (map[r1, c1], map[r2, c2]) = (map[r2, c2], map[r1, c1]);

        public void ToLeft()
        {
            if (coll < 3)
                Swap(row, coll, row, ++coll);
            ++step;
        }
        
        public void ToRight()
        {
            if (coll > 0)
                Swap(row, coll, row, --coll);
            ++step;
        }
        
        public void ToUp()
        {
            if (row < 3)
                Swap(row, coll, ++row, coll);
            ++step;
        }
        
        public void ToDown()
        {
            if (row > 0)
                Swap(row, coll, --row, coll);
            ++step;
        }
        
        public void Mix()
        {
            for (int i = 0; i < 1000; ++i) {
                switch (rnd.Next(4)) {
                    case 0:
                        ToLeft();
                        break;
                    case 1:
                        ToRight();
                        break;
                    case 2:
                        ToUp();
                        break;
                    case 3:
                        ToDown();
                        break;
                }
            }
        }
        
        public bool Win()
        {
            for (int i = 0; i < 4; ++i)
                for (int j = 0; j < 4; ++j)
                    if (map[i, j] != ((i * 4 + j + 1) % 16))
                        return false;
            return true;
        }
    }
}
