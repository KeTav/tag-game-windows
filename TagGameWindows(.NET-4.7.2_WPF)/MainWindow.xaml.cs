﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace TagGameWindows_.NET_4._7._2_WPF_
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ModelGame model = new ModelGame();
        DispatcherTimer dt = new DispatcherTimer();
        DateTime startGame;

        public MainWindow()
        {
            InitializeComponent();

            Refresh();
            dt.Interval = TimeSpan.FromMilliseconds(100);
            dt.Tick += Dt_Tick;
            startGame = DateTime.Now;
            dt.Start();
        }

        private void Dt_Tick(object sender, EventArgs e)
        {
            tbxTimer.Text = (DateTime.Now - startGame).TotalSeconds.ToString("F1");
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key) {
                case Key.Left:
                    model.ToLeft();
                    break;
                case Key.Right:
                    model.ToRight();
                    break;
                case Key.Up:
                    model.ToUp();
                    break;
                case Key.Down:
                    model.ToDown();
                    break;
            }
            tbxStep.Text = model.GetStep.ToString();
            Refresh();
            if (model.Win()) {
                dt.Stop();
                MessageBox.Show($"You won {model.GetStep} steps for {(DateTime.Now - startGame).Minutes}:{(DateTime.Now - startGame).Seconds}!!!");
                model.Init();
                Refresh();
                tbxStep.Text = model.GetStep.ToString();
                startGame = DateTime.Now;
                dt.Start();
            }
        }

        private void Refresh()
        {
            List<List<Cell>> rows = new List<List<Cell>>();
            for (int i = 0; i < 4; ++i) {
                List<Cell> row = new List<Cell>();
                for (int j = 0; j < 4; ++j)
                    if (model.GetCell(i, j) > 0)
                        row.Add(new ChipCell { Num = model.GetCell(i, j) });
                    else
                        row.Add(new SpaceCell());
                rows.Add(row);
            }
            icMap.ItemsSource = rows;
        }
    }

    abstract class Cell
    {

    }

    class ChipCell:Cell
    {
        public int Num { get; set; }
    }

    class SpaceCell:Cell
    {

    }
}
